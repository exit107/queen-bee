import string
import click

@click.command()
@click.option('-l', '--letters', type=click.STRING, required=True, help='Letters available for use, including the required letter.')
@click.option('-r', '--req-letter', type=click.STRING, required=True, help='Required letter (center of puzzle).')
@click.option('-w', '--word-list', type=click.File(), required=True, help='File containing the word list to use.')
def solve_puzzle(letters=None, req_letter=None, word_list=None):
    '''Print out a list of available words and their count.'''
    # Options being passed in
    allowed_letters = set(letters)
    required_letter = req_letter

    # Constants derived from the options
    bad_letters = set(string.ascii_lowercase) - allowed_letters
    word_to_set = lambda x: set(x)
    word_allowed = lambda x: (len(x) >= 4) and (required_letter in x) and x.isdisjoint(bad_letters)

    # Begin solving
    click.echo(f'Solving for letters {allowed_letters} with {required_letter} being required.')
    wls = [word.lower() for word in word_list.read().splitlines() if word_allowed(word_to_set(word.lower()))]
    click.echo(f'{len(wls)} words found.')
    click.echo(wls)

if __name__ == '__main__':
    solve_puzzle()